# Created by xavierlevaux at 04/08/2020
Feature: Customer is able to authenticate

  @wip
  Scenario: Customer not logged in is not authenticated
    Given Customer is not logged in
    When I verify customer authentication
    Then I should receive null