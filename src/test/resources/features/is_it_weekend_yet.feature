# Created by xavierlevaux at 04/08/2020
Feature: Is it weekend yet?
  Are we on weekend?

  Scenario Outline: Today is or is not weekend
    Given today is "<day>"
    When I ask whether it's weekend yet
    Then I should be told "<answer>"

    Examples:
      | day            | answer |
      | Friday         | No     |
      | Saturday       | Yes    |
      | Sunday         | Yes    |
      | anything else! | No     |