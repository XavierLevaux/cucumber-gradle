package be.xl;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class IsItFridayTest {

   @Test
   @DisplayName("When asking on a Monday, should respond 'Nope'")
   public void isItFridayOnAMondayRespondsNope() {
      assertThat(IsItFriday.isItFriday("Monday")).isEqualTo("Nope");
   }

   @Test
   @DisplayName("When asking on a Friday, should respond 'TGIF'")
   public void isItFridayOnAFridayRespondsTGIF() {
      assertThat(IsItFriday.isItFriday("Friday")).isEqualTo("TGIF");
   }

}