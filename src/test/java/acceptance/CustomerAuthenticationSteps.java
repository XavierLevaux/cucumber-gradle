package acceptance;

import io.cucumber.java.PendingException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CustomerAuthenticationSteps {
   @Given("Customer is not logged in")
   public void customerIsNotLoggedIn() {
      throw new PendingException();
   }

   @When("I verify customer authentication")
   public void iVerifyCustomerAuthentication() {
      throw new PendingException();
   }

   @Then("I should receive null")
   public void iShouldReceiveNull() {
      throw new PendingException();
   }
}
