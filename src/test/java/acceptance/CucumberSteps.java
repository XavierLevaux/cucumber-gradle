package acceptance;

import static org.assertj.core.api.Assertions.assertThat;

import be.xl.IsItFriday;
import be.xl.IsItWeekend;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CucumberSteps {
   private String dayOfWeek;
   private String actualAnswer;

   @Given("today is Sunday")
   public void todayIsSunday() {
      dayOfWeek = "Sunday";
   }

   @When("I ask whether it's Friday yet")
   public void iAskWhetherItSFridayYet() {
      actualAnswer = IsItFriday.isItFriday(dayOfWeek);
   }

   @Then("I should be told {string}")
   public void iShouldBeTold(String expectedAnswer) {
      assertThat(expectedAnswer).isEqualTo(actualAnswer);
   }

   @Given("today is Friday")
   public void todayIsFriday() {
      dayOfWeek = "Friday";
   }

   @Given("today is {string}")
   public void todayIs(String day) {
      dayOfWeek = day;
   }

   @When("I ask whether it's weekend yet")
   public void iAskWhetherItSWeekendYet() {
      actualAnswer = IsItWeekend.isItWeekend(dayOfWeek);
   }
}
