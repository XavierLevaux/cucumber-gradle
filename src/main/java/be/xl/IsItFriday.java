package be.xl;

import java.time.DayOfWeek;
import org.apache.commons.lang3.StringUtils;

public class IsItFriday {

   public static String isItFriday(String day) {
      try {
         DayOfWeek dayOfWeek = DayOfWeek.valueOf(StringUtils.upperCase(day));
         return DayOfWeek.FRIDAY == dayOfWeek ? "TGIF" : "Nope";
      } catch (java.lang.IllegalArgumentException iae) {
         return "Nope";
      }
   }

}
