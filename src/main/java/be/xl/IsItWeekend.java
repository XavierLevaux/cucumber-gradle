package be.xl;

import java.time.DayOfWeek;
import org.apache.commons.lang3.StringUtils;

public class IsItWeekend {


   public static String isItWeekend(String day) {
      try {
         DayOfWeek dayOfWeek = DayOfWeek.valueOf(StringUtils.upperCase(day));
         switch (dayOfWeek) {
            case SATURDAY:
            case SUNDAY: return "Yes";
            default: return "No";
         }
      } catch (java.lang.IllegalArgumentException iae) {
         return "No";
      }
   }
}
