# cucumber-gradle

cucumber-gradle is a simple template application demonstrating how to build features using WIP reports.
This is useful when developing wip features on a branch and track progress of those. 

To use WIP features, simply annotate them with "@wip".
Those WIP features should fail until they are implemented.

## Building report

Simple launch a gradle build to have two json reports generated (wip and normal features),
and a complete report buit using (SpacialCircumstances/gradle-cucumber-reporting)[https://github.com/SpacialCircumstances/gradle-cucumber-reporting] plugin.

```bash
./gradlew build
```

## Usage

After running build, reports can found here: build/generated-reports/cucumber-html-reports
And a (single html file)[build/reports/cucumber-report.html] points to them

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
Just as https://github.com/damianszczepanik/cucumber-reporting, and https://github.com/SpacialCircumstances/gradle-cucumber-reporting
this project is available under the terms of the LGPL 2.1 license.